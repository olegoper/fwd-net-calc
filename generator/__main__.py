import random


OPERATIONS = ['+', '-', '/', '*']
MAX_INT = 2 ** 15
MAX_LEN = 5


def rand_op():
    return random.choice(OPERATIONS)


def rand_int():
    return random.randint(0, MAX_INT)


def rand_val(max_len):
    is_expr = random.randint(0, 10) < 3

    if is_expr:
        return '({})'.format(rand_expr(max_len))
    return rand_int()


def rand_expr(max_len):
    max_len = max(2, max_len)
    expr_len = random.randint(2, max_len)

    expr = str(rand_int())

    for _ in xrange(expr_len):
        expr += ' {operation} {value}'.format(
            operation=rand_op(),
            value=rand_val(max_len - 1))

    return expr


def main():
    print rand_expr(MAX_LEN)


if __name__ == '__main__':
    main()
