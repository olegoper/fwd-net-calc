#include <boost/test/unit_test.hpp>

#include <core/lexer.h>
#include <core/symbols_stream.h>

#include <string>
#include <utility>
#include <vector>

using Token = calc::Lexer::Token;

BOOST_AUTO_TEST_CASE(LexerCanParseBasicCharacters) {
    std::vector<std::pair<std::string, Token>> testSet = {
        {"+", Token::Plus},
        {"-", Token::Minus},
        {"*", Token::Multiply},
        {"/", Token::Divide},
        {"(", Token::LeftBracket},
        {")", Token::RightBracket},
        {"\n", Token::NewLine},
    };

    for (auto testItem : testSet) {
        calc::StringSymbolsStream sss(testItem.first);
        calc::Lexer lexer(sss);

        BOOST_TEST(lexer.hasNext());
        BOOST_CHECK_EQUAL(lexer.next(), testItem.second);
        BOOST_TEST(!lexer.hasNext());
    }
}

BOOST_AUTO_TEST_CASE(LexerCanParseNumber) {
    std::vector<std::pair<std::string, int>> testSet = {
        {"1", 1},
        {"23", 23},
        {"456", 456},
        {"7890", 7890},
    };

    for (auto testItem : testSet) {
        calc::StringSymbolsStream sss(testItem.first);
        calc::Lexer lexer(sss);

        BOOST_TEST(lexer.hasNext());
        BOOST_CHECK_EQUAL(lexer.next(), Token::Number);
        BOOST_CHECK_EQUAL(lexer.getLastNumber(), testItem.second);
        BOOST_TEST(!lexer.hasNext());
    }
}

BOOST_AUTO_TEST_CASE(LexerIgnoresWhitespace) {
    std::vector<std::pair<std::string, Token>> testSet = {
        {" +", Token::Plus},
        {"\r-", Token::Minus},
        {"\t*", Token::Multiply},
    };

    for (auto testItem : testSet) {
        calc::StringSymbolsStream sss(testItem.first);
        calc::Lexer lexer(sss);

        BOOST_TEST(lexer.hasNext());
        BOOST_CHECK_EQUAL(lexer.next(), testItem.second);
        BOOST_TEST(!lexer.hasNext());
    }
}

BOOST_AUTO_TEST_CASE(LexerFindsEndOfStream) {
    {
        calc::StringSymbolsStream sss("");
        calc::Lexer lexer(sss);

        BOOST_TEST(!lexer.hasNext());
        BOOST_CHECK_EQUAL(lexer.next(), Token::EndOfStream);
    }

    {
        calc::StringSymbolsStream sss("\t");
        calc::Lexer lexer(sss);

        BOOST_TEST(lexer.hasNext());
        BOOST_CHECK_EQUAL(lexer.next(), Token::EndOfStream);
        BOOST_TEST(!lexer.hasNext());
    }
}

BOOST_AUTO_TEST_CASE(LexerCanParseComplexExpression) {
    calc::StringSymbolsStream sss("11 + 2-\t(");
    calc::Lexer lexer(sss);

    BOOST_TEST(lexer.hasNext());
    BOOST_CHECK_EQUAL(lexer.next(), Token::Number);
    BOOST_CHECK_EQUAL(lexer.getLastNumber(), 11);

    BOOST_TEST(lexer.hasNext());
    BOOST_CHECK_EQUAL(lexer.next(), Token::Plus);

    BOOST_TEST(lexer.hasNext());
    BOOST_CHECK_EQUAL(lexer.next(), Token::Number);
    BOOST_CHECK_EQUAL(lexer.getLastNumber(), 2);

    BOOST_TEST(lexer.hasNext());
    BOOST_CHECK_EQUAL(lexer.next(), Token::Minus);

    BOOST_TEST(lexer.hasNext());
    BOOST_CHECK_EQUAL(lexer.next(), Token::LeftBracket);
}

BOOST_AUTO_TEST_CASE(LexerThrowsOnBadCharacter) {
    std::vector<std::string> testSet = {"a", "b11", " {", "." };

    for (auto testItem : testSet) {
        calc::StringSymbolsStream sss(testItem);
        calc::Lexer lexer(sss);

        BOOST_TEST(lexer.hasNext());
        BOOST_CHECK_THROW(lexer.next(), calc::BadToken);
    }
}

BOOST_AUTO_TEST_CASE(LexerFindsNumberBoundaries) {
    calc::StringSymbolsStream sss("11 + 2\n-66\n");
    calc::Lexer lexer(sss);

    BOOST_TEST(lexer.hasNext());
    BOOST_CHECK_EQUAL(lexer.next(), Token::Number);
    BOOST_CHECK_EQUAL(lexer.getLastNumber(), 11);

    BOOST_TEST(lexer.hasNext());
    BOOST_CHECK_EQUAL(lexer.next(), Token::Plus);

    BOOST_TEST(lexer.hasNext());
    BOOST_CHECK_EQUAL(lexer.next(), Token::Number);
    BOOST_CHECK_EQUAL(lexer.getLastNumber(), 2);

    BOOST_TEST(lexer.hasNext());
    BOOST_CHECK_EQUAL(lexer.next(), Token::NewLine);

    BOOST_TEST(lexer.hasNext());
    BOOST_CHECK_EQUAL(lexer.next(), Token::Minus);

    BOOST_TEST(lexer.hasNext());
    BOOST_CHECK_EQUAL(lexer.next(), Token::Number);
    BOOST_CHECK_EQUAL(lexer.getLastNumber(), 66);

    BOOST_TEST(lexer.hasNext());
    BOOST_CHECK_EQUAL(lexer.next(), Token::NewLine);

    BOOST_TEST(!lexer.hasNext());
}

