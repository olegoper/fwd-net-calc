#include <boost/test/unit_test.hpp>

#include <core/expression.h>

#include <string>
#include <tuple>
#include <utility>
#include <vector>

using Token = calc::Lexer::Token;

BOOST_AUTO_TEST_CASE(StreamBasicExpressionCanCalculateOperations) {
    std::vector<std::tuple<int, Token, int, double>> testSet = {
        {7, Token::Plus, 3, 10},
        {7, Token::Minus, 3, 4},
        {7, Token::Multiply, 3, 21},
        {5, Token::Divide, 2, 2.5},
    };

    for (auto testItem : testSet) {
        calc::StreamBasicExpression expr;

        int left;
        Token operation;
        int right;
        double res;
        std::tie(left, operation, right, res) = testItem;
        expr.addNumber(left);
        expr.addToken(operation);
        expr.addNumber(right);

        BOOST_CHECK_EQUAL(expr.getResult(), res);
    }
}

BOOST_AUTO_TEST_CASE(StreamExpressionCanCalculateOperations) {
    std::vector<std::tuple<int, Token, int, double>> testSet = {
        {7, Token::Plus, 3, 10},
        {7, Token::Minus, 3, 4},
        {7, Token::Multiply, 3, 21},
        {5, Token::Divide, 2, 2.5},
    };

    for (auto testItem : testSet) {
        calc::StreamExpression expr;

        int left;
        Token operation;
        int right;
        double res;
        std::tie(left, operation, right, res) = testItem;
        expr.addNumber(left);
        expr.addToken(operation);
        expr.addNumber(right);
        expr.addToken(Token::EndOfStream);

        BOOST_CHECK_EQUAL(expr.getResult(), res);
    }
}

BOOST_AUTO_TEST_CASE(StreamExpressionFinishesOnNewLine) {
    calc::StreamExpression expr;
    expr.addNumber(1);
    expr.addToken(Token::Plus);
    expr.addNumber(2);
    expr.addToken(Token::NewLine);
    BOOST_TEST(expr.isFinished());
    BOOST_CHECK_NO_THROW(expr.getResult());
}

BOOST_AUTO_TEST_CASE(StreamExpressionFinishesOnEndOfStream) {
    calc::StreamExpression expr;
    expr.addNumber(1);
    expr.addToken(Token::Plus);
    expr.addNumber(2);
    expr.addToken(Token::EndOfStream);
    BOOST_TEST(expr.isFinished());
    BOOST_CHECK_NO_THROW(expr.getResult());
}

BOOST_AUTO_TEST_CASE(StreamExpressionNotFinishedGetResultThrows) {
    calc::StreamExpression expr;
    expr.addNumber(1);
    expr.addToken(Token::Plus);
    expr.addNumber(2);
    BOOST_CHECK_THROW(expr.getResult(), calc::BadOperation);
}

BOOST_AUTO_TEST_CASE(StreamExpressionCanCalculateLongOperations) {
    calc::StreamExpression expr;
    expr.addNumber(1);
    expr.addToken(Token::Plus);
    expr.addNumber(6);
    expr.addToken(Token::Minus);
    expr.addNumber(20);
    expr.addToken(Token::Multiply);
    expr.addNumber(3);
    expr.addToken(Token::EndOfStream);
    BOOST_CHECK_EQUAL(expr.getResult(), -39);
}

BOOST_AUTO_TEST_CASE(StreamExpressionWithTwoConequentOperationsThrows) {
    calc::StreamExpression expr;
    expr.addNumber(1);
    expr.addToken(Token::Plus);
    BOOST_CHECK_THROW(expr.addToken(Token::Plus), calc::BadToken);
}

BOOST_AUTO_TEST_CASE(StreamExpressionWithTwoConequentNumbersThrows) {
    calc::StreamExpression expr;
    expr.addNumber(1);
    BOOST_CHECK_THROW(expr.addNumber(2), calc::BadToken);
}

BOOST_AUTO_TEST_CASE(StreamExpressionWithEmptyBracketsThrows) {
    calc::StreamExpression expr;
    expr.addToken(Token::LeftBracket);
    BOOST_CHECK_THROW(expr.addToken(Token::RightBracket), calc::BadToken);
}

BOOST_AUTO_TEST_CASE(StreamExpressionCanStartWithSign) {
    {
        calc::StreamExpression expr;
        expr.addToken(Token::Plus);
        expr.addNumber(1);
        expr.addToken(Token::Plus);
        expr.addNumber(2);
        expr.addToken(Token::EndOfStream);
        BOOST_CHECK_EQUAL(expr.getResult(), 3);
    }
    {
        calc::StreamExpression expr;
        expr.addToken(Token::Minus);
        expr.addNumber(1);
        expr.addToken(Token::Plus);
        expr.addNumber(2);
        expr.addToken(Token::EndOfStream);
        BOOST_CHECK_EQUAL(expr.getResult(), 1);
    }
}

BOOST_AUTO_TEST_CASE(StreamExpressionStartWithOperationThrows) {
    std::vector<Token> testSet = {
        Token::Number,
        Token::Multiply,
        Token::Divide,
        Token::RightBracket,
        Token::NewLine,
        Token::EndOfStream
    };
    for (auto testItem : testSet) {
        calc::StreamExpression expr;
        BOOST_CHECK_THROW(expr.addToken(testItem), calc::BadToken);
    }
}

BOOST_AUTO_TEST_CASE(StreamExpressionCanCalculateSubexpression) {
    calc::StreamExpression expr;
    expr.addNumber(1);
    expr.addToken(Token::Plus);
    expr.addToken(Token::LeftBracket);
    expr.addNumber(2);
    expr.addToken(Token::Plus);
    expr.addNumber(3);
    expr.addToken(Token::RightBracket);
    expr.addToken(Token::Plus);
    expr.addNumber(4);
    expr.addToken(Token::EndOfStream);
    BOOST_CHECK_EQUAL(expr.getResult(), 10);
}

BOOST_AUTO_TEST_CASE(StreamExpressionCanStartWithSubexpression) {
    calc::StreamExpression expr;
    expr.addToken(Token::LeftBracket);
    expr.addNumber(1);
    expr.addToken(Token::Plus);
    expr.addNumber(2);
    expr.addToken(Token::RightBracket);
    expr.addToken(Token::Plus);
    expr.addNumber(3);
    expr.addToken(Token::EndOfStream);
    BOOST_CHECK_EQUAL(expr.getResult(), 6);
}

BOOST_AUTO_TEST_CASE(StreamExpressionCanEndWithSubexpression) {
    calc::StreamExpression expr;
    expr.addNumber(1);
    expr.addToken(Token::Plus);
    expr.addToken(Token::LeftBracket);
    expr.addNumber(2);
    expr.addToken(Token::Plus);
    expr.addNumber(3);
    expr.addToken(Token::RightBracket);
    expr.addToken(Token::EndOfStream);
    BOOST_CHECK_EQUAL(expr.getResult(), 6);
}

BOOST_AUTO_TEST_CASE(StreamExpressionCanCalculateMultipleSubexpressions) {
    calc::StreamExpression expr;
    expr.addNumber(1);
    expr.addToken(Token::Plus);
    expr.addToken(Token::LeftBracket);
    expr.addNumber(2);
    expr.addToken(Token::Plus);
    expr.addToken(Token::LeftBracket);
    expr.addNumber(3);
    expr.addToken(Token::Plus);
    expr.addNumber(4);
    expr.addToken(Token::RightBracket);
    expr.addToken(Token::RightBracket);
    expr.addToken(Token::EndOfStream);
    BOOST_CHECK_EQUAL(expr.getResult(), 10);
}
