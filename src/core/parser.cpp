#include "parser.h"

namespace calc {

std::vector<double> StreamParser::calculate(Lexer& lexer) {
    std::vector<double> res;

    while (lexer.hasNext()) {
        Lexer::Token token = lexer.next();
        if (Lexer::EndOfStream == token) {
            continue;
        }
        if (Lexer::Number == token) {
            expression_->addNumber(lexer.getLastNumber());
        } else {
            expression_->addToken(token);
        }

        if (expression_->isFinished()) {
            res.push_back(expression_->getResult());
            expression_.reset(new StreamExpression);
        }
    }

    return res;
}

double StreamParser::endOfStream() {
    expression_->addToken(Lexer::EndOfStream);
    double res = expression_->getResult();
    expression_.reset(new StreamExpression);
    return res;
}

} // namespace calc

