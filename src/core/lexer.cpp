#include "lexer.h"

#include <cctype>

namespace calc {

char Lexer::tokenToChar(Token token) {
    const std::unordered_map<Token, char> tokens = {
        {Plus, '+'},
        {Minus, '-'},
        {Multiply, '*'},
        {Divide, '/'},
        {LeftBracket, '('},
        {RightBracket, ')'},
        {NewLine, '\n'},
    };
    auto it = tokens.find(token);
    if (it != tokens.end()) {
        return it->second;
    }
    throw BadToken(std::string("Unexpected lexer token \"") + std::to_string(token) + "\"");
}

Lexer::Token Lexer::next() {
    Token token = nextRaw();
    while (token == Whitespace) {
        token = nextRaw();
    }
    return token;
}

Lexer::Token Lexer::nextRaw() {
    if (streaming_) {
        streaming_ = false;
        return parseSymbol(lastChar_);
    }

    if (!stream_.hasNext()) {
        return EndOfStream;
    }

    char symbol = stream_.next();
    Token token = parseSymbol(symbol);
    if (token != Number) {
        return token;
    }
    lastNumber_ = symbolToDigit(symbol);

    while (true) {
        if (!stream_.hasNext()) {
            break;
        }
        char symbol = stream_.next();
        if (parseSymbol(symbol) != Number) {
            lastChar_ = symbol;
            streaming_ = true;
            break;
        }
        lastNumber_ *= 10;
        lastNumber_ += symbolToDigit(symbol);
    }
    return Number;
}

Lexer::Token Lexer::parseSymbol(char symbol) {
    static const std::unordered_map<char, Token> symbols = {
        {'+', Plus},
        {'-', Minus},
        {'*', Multiply},
        {'/', Divide},
        {'(', LeftBracket},
        {')', RightBracket},
        {'\n', NewLine},
        {'\0', EndOfStream},
    };
    if (std::isdigit(symbol)) {
        return Number;
    }
    auto it = symbols.find(symbol);
    if (it != symbols.end()) {
        return it->second;
    }
    if (std::isblank(symbol) || '\r' == symbol) {
        return Whitespace;
    }

    throw BadToken(std::string("Unexpected token \"") + symbol + "\"");
}

} // namespace calc

