#pragma once

#include <cstddef>
#include <iostream>
#include <sstream>
#include <string>

namespace calc {

class SymbolsStream {
public:
    virtual bool hasNext() = 0;
    virtual char next() = 0;
    virtual ~SymbolsStream() = default;
};

class InputSymbolsStream: public SymbolsStream {
public:
    InputSymbolsStream(std::istream& data):
        data_(data)
    {
    }

    bool hasNext() override {
        return static_cast<bool>(data_);
    }

    char next() override;

private:
    std::istream& data_;
};

class StringSymbolsStream: public SymbolsStream {
public:
    StringSymbolsStream(const std::string& data):
        data_(data),
        next_(0)
    {
    }

    bool hasNext() override {
        return next_ < data_.size();
    }

    char next() override {
        return data_[next_++];
    }

private:
    std::string data_;
    size_t next_;
};

} // namespace calc

