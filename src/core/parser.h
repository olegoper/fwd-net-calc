#pragma once

#include "expression.h"
#include "lexer.h"

#include <cstdint>
#include <memory>
#include <stdexcept>
#include <vector>

namespace calc {

class StreamParser {
public:
    StreamParser(): expression_(new StreamExpression)
    {
    }

    std::vector<double> calculate(Lexer& lexer);

    double endOfStream();

private:
    std::unique_ptr<StreamExpression> expression_;
};

} // namespace calc

